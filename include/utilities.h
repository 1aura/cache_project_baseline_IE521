/*
 * Cache simulation project
 * Class UCR IE-521
 *
 * Autores Originales:
 * 		Julian Morua Vindas B54872
 * 		José López Picado
 * 
 * Modificaciones Por:
 * 		Laura Rojas B76798
*/

#ifndef UTILITIES
#define UTILITIES


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include "L1cache.h"

#define BUFFER_SIZE 32
#define VC_SIZE 16
#define ADDRESS_SIZE 32
#define FACT_ASOC_L2 2
#define FACT_C_SIZE_L2 4
#define RAND -1
typedef unsigned int u_int;

enum optimization_types{
  L2,
  VC
};

/*
 * Código de manejo de parámetros
 */

/**
 *	\struct	parameters
 *	\var	t es el tamaño de cache en kilobytes
 * 	\var	l es el tamaño de línea en bytes
 * 	\var	a es la asociatividad
 * 	\var	rp es la política de reemplazo
 * 	\var	opt es la optimización, victim cache o cache multinivel
 *	\brief 	Contiene todos los parámetros como 'int' en un struct. Valor por defecto es 0.
 */
typedef struct{
  u_int t;   //size of the cache in kilobytes.
  u_int l;   //size of the cache block in bytes.
  u_int a;   //associativity.
  u_int rp;  //replacement policy
  u_int opt; //optimization: victim cache or maltilevel cache.
} parameters;

/**
 *	\fn 	readInput
 *	\param	string es un puntero de char que indica el string a leer.
 *	\return	salida contiene el entero equivalente al string indicado.
 *	\brief 	Contiene un 'parser' para leer el contenido de uno de los argumentos de línea de comando, y traducirlo a un entero.
 */
u_int readInput(char* string);

/**
 * \fn		getParams
 * \param	argc es la cantidad de argumentos
 * \param	argv es el puntero a los argumentos
 * \return	params es un struct con todos los parametros ingresados por el usuairo
 * \brief	Obtiene todos los argumentos de línea de comando y los guarda en un struct de tipo 'parametros'.
 */
parameters getParams(int argc, char* argv[]);

/**
 * \fn		printParams
 * \param	params es un struct con todos los parametros necesarios
 * \brief	Imprime en consola la parte indicada por los parametros.
 */
void printParams(parameters params);

/**
 * \fn		printStats
 * \param	ML un entero que indica la cantidad de MISS_LOADs
 * \param	MS un entero que indica la cantidad de MISS_STOREs
 * \param	HL un entero que indica la cantidad de HIT_LOADs
 * \param	HS un entero que indica la cantidad de HIT_STOREs
 * \param	DE un entero que indica la cantidad de 'dirty evictions'
 * \brief	Imprime en consola las estadisticas totales del cache
 */
void printStats(int ML, int MS, int HL, int HS, int DE);


/*
 * Código para manejo de direcciones
 */
 
/**
 *	\struct	sizes
 *	\var	l1_tag_bits es la cantidad de bits en el tag del cache L1
 * 	\var	l1_offset_bits es la cantidad de bits en el offset del cache L1
 * 	\var	l1_index_bits es la cantidad de bits en el indice del cache L1
 * 	\var	l2_tag_bits es la cantidadde bits en el tag del cachce L2
 * 	\var	l2_offset_bits es la cantidad de bits en el offset del cache L2
 * 	\var	l2_index_bits es la cantidad de bits en el indice del cache L2
 * 	\var	vc_assoc es la asociatividad para el cache víctima
 * 	\var	l1_assoc es la asociatividad para el cache L1
 * 	\var	l2_assoc es la asociatividad para el cache L2
 *	\brief 	Contiene todos los parámetros como 'int' en un struct. Valor por defecto es 0.
 */
typedef struct{
  u_int l1_tag_bits;    //
  u_int l1_offset_bits; //
  u_int l1_index_bits;  //
  u_int l2_tag_bits;    //
  u_int l2_offset_bits; //
  u_int l2_index_bits;  //
  u_int vc_assoc;       //
  u_int l1_assoc;
  u_int l2_assoc;
}sizes;

/**
 *	\struct	line_info
 *	\var	address es la dirección completa
 * 	\var	loadstore es la indicación de si fue una instrucción load o store
 * 	\var	ic es la indicación ic, que se utiliza para calcular el tiempo completo de duración
 *	\brief 	Contiene toda la información de una línea de un archivo .trace
 */
typedef struct {
  u_int address;    //
  u_int loadstore;  //
  u_int ic;         //
}line_info;

/**
 *	\struct	sizes
 *	\var	original_address es la dirección original
 * 	\var	l1_tag es el tag para el cache L1
 * 	\var	l1_idx es el índice para el cache L1
 * 	\var	l1_offset es el offset para el cache L1
 * 	\var	l1_assoc es la asociatividad para el cache L1
 * 	\var	l2_tag es el tag para el cache L2
 * 	\var	l2_idx es el índice para el cache L2
 * 	\var	l2_offset es el offset para el cache L2
 * 	\var	l2_assoc es la asociatividad para el cache L2
 * 	\var	vc_assoc es la asociatividad para el cache víctima
 *	\brief 	Contiene toda la información de entrada indicada por la dirección del archivo .trace
 */
typedef struct{
  u_int original_address;
  // L1
	u_int l1_tag;
	u_int l1_idx;
	u_int l1_offset;
	u_int l1_assoc;
  // L2
	u_int l2_tag;
	u_int l2_idx;
	u_int l2_offset;
	u_int l2_assoc;
  // VC
  u_int vc_assoc;
}entry_info;

/**
 * \fn		get_sizes
 * \param	params es un struct que contiene todos los parámetros de la simulación
 * \return	sizes es un struct que contiene todos los tamaños necesarios.
 * \brief	Obtiene los tamaños para todas las áreas en donde una dirección de memoria se convierte a un bloque en cache
 */
sizes get_sizes(parameters params);

/**
 * \fn		print_sizes
 * \param	sizes es un struct que contiene todos los tamaños necesarios
 * \param 	opt es la optimización usada para filtrar los datos
 * \brief	Es una función que imprime todos los tamaños del struct sizes.
 */
void print_sizes(sizes sizes, int opt);

/**
 * \fn		get_entry_info 
 * \param	address es la dirección de la línea leida
 * \return	entry_info es un struct con la información de entrada leida correctamente
 * \brief 	Obtiene toda la información de entrada
 */
entry_info get_entry_info(long address, sizes sizes);

/**
 * \fn		get_line_info
 * \param	line es el string de la línea actual del archivo trace
 * \param	token es un token que permite hacer lectura de esa línea usando strtok
 * \param	status es el estatus de la línea
 * \return	info es el struct que contiene toda la información de la línea
 * \brief	Obtiene toda la información necesaria de una línea del archivo .trace
 */
line_info get_line_info(char* line, char* token, char* status);

/*
 * Código para imprimir
 */

/**
 * \fn		print_entry_info
 * \param	info es un struct que contiene la información de entradaa
 * \param	opt es la optimización usada para filtrar los datos
 * \brief	Imprime la información de entrada
 */
void print_entry_info(entry_info info, int opt);

/**
 * \fn		print_address
 * \param	addr es la dirección a imprimir
 * \param	msj es el mensaje a imprimir
 * \brief	Imprime una dirección
 */
void print_address(u_int addr, const char* msj="");

/**
 * \fn		print_entry
 * \param	block es la entrada de cache que se tiene que imprimir
 * \param	msj es el mensaje que se debe imprimir
 * \brief	Imprime los datos indicados por una entrada de cache
 */
void print_entry(entry block,const char* msj="");

/**
 * \fn		print_set
 * \param	set es el set de cache que se tiene que imprimir
 * \param	msj es el mensaje que se necesita imprimir
 * \brief	Imprime los datos de un set de cache.
 */
void print_set(entry* set, int assoc, const char* msj="");

/**
 * \fn		print_result
 * \param	results es un struct de resultado cuya información se necesita imprimir
 * \param 	msj es el mensaje a imprimir
 * \brief	Imprime la operación resultante de una referencia a memoria
 */
void print_result(operation_result results,const char* msj="");

#endif
