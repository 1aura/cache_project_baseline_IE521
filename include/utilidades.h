/*
 * Funciónes útiles para proyecto de simulación de cache
 * IE-521
 *
 * Autoras:
 * 	Laura Rojas - B76798
 * 	Carolina
 * 	Michelle
*/

#ifndef UTILIDADES
#define UTILIDADES

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

/*
 * Código de manejo de parámetros:
 */

/**
 *	\struct	input_params
 *	\var	t es el tamaño de cache en kilobytes
 * 	\var	l es el tamaño de línea en bytes
 * 	\var	a es la asociatividad
 * 	\var	rp es la política de reemplazo
 *	\brief 	Contiene todos los parámetros como 'int' en un struct. Valor por defecto es 0.
 */
typedef struct{
  int t; 
  int l;  
  int a; 
  int rp; 
} input_params;

/**
 *	\fn 	readInput
 *	\param	string es un puntero de char que indica el string a leer.
 *	\return	salida contiene el entero equivalente al string indicado.
 *	\brief 	Contiene un 'parser' para leer el contenido de uno de los argumentos de línea de comando, y traducirlo a un entero.
 */
int readInput(char* string);

/**
 * \fn		getParams
 * \param	argc es la cantidad de argumentos
 * \param	argv es el puntero a los argumentos
 * \return	params es un struct con todos los parametros ingresados por el usuairo
 * \brief	Obtiene todos los argumentos de línea de comando y los guarda en un struct de tipo 'parametros'.
 */
input_params getParams(int argc, char* argv[]);

/**
 * \fn		printParams
 * \param	params es un struct con todos los parametros necesarios
 * \brief	Imprime en consola la parte indicada por los parametros.
 */
void printParams(input_params params);

/*
 * Código de manejo de dirección:
 */
 
/**
 * \struct	address
 * \var		full es la dirección completa
 * \var		tag es el tag de una dirección particular
 * \var		offset el offset de una dirección particular
 * \var		index es el índice de una dirección particular
 * \var		a es la asociatividad para esa dirección
 * \brief	Contiene la información de una dirección, completa y dividida en categorías.
 */
typedef struct{
	unsigned int full;
	unsigned int tag;
	unsigned int offset;
	unsigned int index;
}address;

/**
 * \struct	address_sizes
 * \var		tag_bits es el número de bits utilizado para el tag
 * \var		offset_bits es el número de bits utilizados para el offset
 * \var		index_bits es el número de bits utilizados para el índice
 * \brief	Contiene todos los tamaños para realizar el cache.
 */
typedef struct{
	int tag_bits;
	int offset_bits;
	int index_bits;
}address_sizes;

/**
 * \fn		getSizes
 * \param	params es un struct con todos los parametros necesarios
 * \return	sizes es un struct con todos los tamaños necesarios para el cache
 * \brief 	Obtiene los tamaños necesarios para procesar una dirección
 */
address_sizes getSizes(input_params params);

/**
 * \fn		getAddress
 * \param	input es la dirección original, sin procesar
 * \param	sizes son los tamaños indicados de la dirección
 * \param	ptr es el puntero a la dirección indicada
 * \brief	Actualiza el contenido de un address utilizando una dirección nueva.
 */
void getAddress(unsigned int input, address_sizes sizes, address* ptr);

#endif





