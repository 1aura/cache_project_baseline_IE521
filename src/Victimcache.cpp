/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_replacement_policy_l1_vc(const entry_info *l1_vc_info,
    	                      	 bool loadstore,
       	                    	 entry* l1_cache_blocks,
								 entry* vc_cache_blocks,
								 operation_result* l1_result,
              	              	 operation_result* vc_result,
								 bool debug)
{
	// obtener bloques a mover
	entry MRU = l1_cache_blocks[(*l1_vc_info).l1_assoc - 1];
	entry TEMP;
	int i;
	int end = 16;
	
	// llamar lru
	int status = lru_replacement_policy((*l1_vc_info).l1_idx, 
									(*l1_vc_info).l1_tag,
									(*l1_vc_info).l1_assoc,
									loadstore,
									l1_cache_blocks,
									l1_result,
									debug);
									
	
	// caso cache hit
	if ( ((*l1_result).miss_hit == HIT_LOAD) || ((*l1_result).miss_hit == HIT_STORE) ) {
		// hacer nada
	}
	// caso cache miss
	else{ // cache tiene miss, 2 casos:
		// Asumir un miss
		if(loadstore) { // si loadstore == true (1) es un store
			(*vc_result).miss_hit = MISS_STORE;
		}
		else { // loadstore == false (0) es un load
			(*vc_result).miss_hit = MISS_LOAD;
		}
		// Revisar todo victim
		for (i = 0; i < 16; i++){
			//printf("I got here\n");
			// Hay un hit?
			if(vc_cache_blocks[i].tag == (*l1_vc_info).l1_tag){
				// Actualizar end y TEMP
				end = i;
				MRU = vc_cache_blocks[i];
				// REVISAR LOADSTORE
				if(loadstore) { // si loadstore == true (1) es un store
					(*vc_result).miss_hit = HIT_STORE;
				}
				else { // loadstore == false (0) es un load
					(*vc_result).miss_hit = HIT_LOAD;
				}
			}
		}
		
		// REEMPLAZAR VICTIMCACHE
		for (i = end-1; i >= 0; i --){
			vc_cache_blocks[i+1] = vc_cache_blocks[i];
		}
		vc_cache_blocks[0] = MRU;

	}
   
   return OK;
}

