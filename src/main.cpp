/*
 * Programa principal para proyecto de simulación de cache
 * IE-521
 *
 * Autoras:
 * 	Laura Rojas - B76798
 * 	Carolina
 * 	Michelle
*/

// incluir librerias por defecto
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <math.h>
// incluir archivos de cache
#include <L1cache.h>
// incluir utilidades
#include <debug_utilities.h>
#include <utilities.h>

/* Helper funtions , borrar? No tiene utilidad...
void print_usage ()
{
  printf ("Print print_usage\n");
  exit (0);
}*/

// Para depuración:
bool DEBUG_PRINT = false;


int main(int argc, char *argv[])
{
	// Declaración de variables
	static char line[80];
	char* token, *status;
	line_info info;
	entry_info direccion;
	int error;
	operation_result result;
	// Contadores
	int ML = 0;
	int MS = 0;
	int HL = 0;
	int HS = 0;
	int DE = 0;
	
	// Obtener parametros y tamaños de dirección basado en estos parámetros
	parameters params = getParams(argc,argv);
	sizes cacheSizes = get_sizes(params);
	
	// Hacer Cache [  (tamaño / línea*asociatividad)   x asociatividad]
	entry cache [((params.t*1024)/(params.l*params.a))][cacheSizes.l1_assoc]; 
		
	// Imprimir parametros
	printParams(params);
	
	// Imprimir tamaños (solo para depurar)
	if(DEBUG_PRINT){
		print_sizes(cacheSizes, 0);
	}
	
	// Leer todo el archivo trace. Mientras la variable 'status' no sea nula, se sigue revisando línea por línea.
	do {
		// Utilizar fgets y revisar condición de salida
		status = fgets(line,80,stdin);
		if (status == NULL){
			break;
		}
		// Leer una línea del archivo .trace
		info = get_line_info(line, token, status);
		direccion = get_entry_info(info.address, cacheSizes);
		// Solo para depurar lectura de archivo .trace
		if(DEBUG_PRINT){ 
			// printf("%u %x %u\n",info.loadstore, info.address, info.ic); 
			// print_entry_info(direccion, 0);
		}
		
		// Elegir política de reemplazo
		if(params.rp == 1){
			error = lru_replacement_policy 	   (direccion.l1_idx, 
												direccion.l1_tag, 
												direccion.l1_assoc, 
												(info.loadstore == 1), 
												cache[direccion.l1_idx],
												&result,
												DEBUG_PRINT);
			// revisar contadores
			if (result.miss_hit == MISS_STORE){
				MS ++;
			}
			else if (result.miss_hit == MISS_LOAD){
				ML ++;
			}
			else if (result.miss_hit == HIT_STORE){
				HS ++;
			}
			else {
				HL ++;
			}
			// revisar dirty evictions
			if(result.dirty_eviction == 1){
				DE ++;
			}
		}
		else if (params.rp == 2){
			error = nru_replacement_policy	   (direccion.l1_idx, 
												direccion.l1_tag, 
												direccion.l1_assoc, 
												(info.loadstore == 1), 
												cache[direccion.l1_idx],
												&result,
												DEBUG_PRINT);
						// revisar contadores
			if (result.miss_hit == MISS_STORE){
				MS ++;
			}
			else if (result.miss_hit == MISS_LOAD){
				ML ++;
			}
			else if (result.miss_hit == HIT_STORE){
				HS ++;
			}
			else {
				HL ++;
			}
			// revisar dirty evictions
			if(result.dirty_eviction == 1){
				DE ++;
			}
		}
		else if (params.rp == 3){
			error = srrip_replacement_policy   (direccion.l1_idx, 
												direccion.l1_tag, 
												direccion.l1_assoc, 
												(info.loadstore == 1), 
												cache[direccion.l1_idx],
												&result,
												DEBUG_PRINT);
			// revisar contadores
			if (result.miss_hit == MISS_STORE){
				MS ++;
			}
			else if (result.miss_hit == MISS_LOAD){
				ML ++;
			}
			else if (result.miss_hit == HIT_STORE){
				HS ++;
			}
			else {
				HL ++;
			}
			// revisar dirty evictions
			if(result.dirty_eviction == 1){
				DE ++;
			}
		}
		else{
			//printf("Eliga una política de reemplazo: \n\tLRU 1 \n\tNRU 2 \n\tSRRIP 3 \n");
		}
		/**
		 * CÓDIGO QUE HACE FALTA -> SIMULACIÓN DE CACHE 
		 */
		 
	} while (status != NULL);
	
	if(DEBUG_PRINT){
		printf("MISS_STORE: %d, MISS_LOAD: %d, HIT_STORE: %d, HIT_LOAD: %d DIRTY_EVICTIONS: %d\n",MS,ML,HS,HL,DE);
	}
	
	// Imprimir estadísticas
	printStats(ML,MS,HL,HS,DE);
	
	

	/* Print Statistics */
	return 0;
}
