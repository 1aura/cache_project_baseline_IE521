/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int field_size_get(struct cache_params cache_params,
                   struct cache_field_size *field_size)
{
 
  return OK;
}

void address_tag_idx_get(long address,
                         struct cache_field_size field_size,
                         int *idx,
                         int *tag)
{


}

int srrip_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
// Reconocer parámetros no válidos
if (idx < 0 || associativity < 0 || tag < 0 ){
	return PARAM;
}

  // Variables de interes para esta política de remplazo 
  int M = (associativity <= 2) ? 1:2;
  int rpv_to_search = pow (2, M) - 1;
  int rpv_new = pow (2, M) -2;
  int free_space = -1;
  int wanted_space = -1;
  bool hit = false;
  int pos_hit;
  bool miss = false;
 
  // Revisar las vías 
  for (int i = 0; i < associativity; i++)
  {
	// Revisar si hay espacio libre  
	  if (cache_blocks[i].valid == false)
	  {
		  if (free_space == -1)
		  {
			  free_space = i;
		  }
	  }
   // Revisar si hay algún bloque con el rpv igual al buscado
       if (cache_blocks[i].rp_value == rpv_to_search && wanted_space == -1)
	  {
		  wanted_space = i; // Posición del bloque a remplazar en caso de miss sin espacio libre
	  }    
  
  // Revisar si hay un Hit o un Miss
  if (cache_blocks[i].tag == tag && cache_blocks[i].valid == true) // Condiciones para Hit
  {
    hit = true;
	pos_hit = i;
	
  } else // Si no hay Hit hay Miss
  {
  miss = true;

  } // miss
  //return OK;
} // for

 if (hit){
	cache_blocks[pos_hit].rp_value = 0; // -> RRPV = 0
	// Actulizar estadísticas
	if (loadstore)
	{
     result -> miss_hit = HIT_STORE;
	} else {
	 result -> miss_hit = HIT_LOAD;	
	}
	result->dirty_eviction = false;
   // Actualizar bit de dirty solo si no estaba sucio antes
   if (cache_blocks[pos_hit].dirty != 1 ) {
    if (loadstore)
	{
		cache_blocks[pos_hit].dirty = true;
	} 
   } 
		return OK;
}

if (miss){
		// Actulizar estadísticas
	if (loadstore)
	{
     result -> miss_hit = MISS_STORE;
	} else {
	 result -> miss_hit = MISS_LOAD;	
	}
	// Verifico si había encontrado un espacio libre
  if (free_space != -1)
  {
	// Si hay espacio libre se procede a usarlo
	cache_blocks[free_space].tag = tag;
	cache_blocks[free_space].rp_value = rpv_new; // RRPV = 2 ^ M -2
    cache_blocks[free_space].valid = true;
	// Actualizar bit de dirty 
    if (loadstore)
	{
		cache_blocks[free_space].dirty = true;
	}	else 
		{
		cache_blocks[free_space].dirty = false;	
		}
    return OK;
  } else // No se encontró espacio libre, se utiliza la política para elegir el bloque a remplazar 
    {
	  if (wanted_space == -1) // No hay ningún bloque con el rpv deseado
	  {
      bool bloqueEncontrado = false;
      // Se debe sumar 1 a todos los bloques del set
	    while (!bloqueEncontrado)
	   {
       for (int i=0; i < associativity; i++)
	    {
          cache_blocks[i].rp_value++;
		  // Revisa si ahora encuentra el rpv deseado
		  if (cache_blocks[i].rp_value == rpv_to_search && wanted_space == -1)
		   {
             wanted_space = i;
		   }
	    }
		 // Si se suma 1 a los rrpv de los bloques y aún no se encuentra el valor deseado, se repite
		   if (wanted_space != -1)
		    {
			  bloqueEncontrado = true;   
		    }
	   }
	  } 
	   // Información del bloque a remplazar
	result -> evicted_address = cache_blocks[wanted_space].tag;
	result -> dirty_eviction = cache_blocks[wanted_space].dirty;
	 // Se remplaza la dirección correspondiente
    cache_blocks[wanted_space].tag = tag;
	// Se actualiza bit de dirty 
     if (loadstore)
	{
		cache_blocks[wanted_space].dirty = true;
	}	else 
		{
		cache_blocks[wanted_space].dirty = false;	
		}
	// Se actualiza el valor de rrpv y valid
	    cache_blocks[wanted_space].rp_value = rpv_new; // RRPV = 2 ^ M -2
		cache_blocks[wanted_space].valid = true;
		return OK; 
    }  
}
   return ERROR;
} 


int lru_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
	// Declaración de variables;
	int i;
	
	// Revisar test 6
	if (idx < 0 ||tag < 0 || associativity < 0){
		return PARAM; 
	}
	
	// Inicializar 'end' como el final de la lista
	int end = associativity-1;
	bool current_dirty = 0;
	
	// Asumir un miss
	if(loadstore) { // si loadstore == true (1) es un store
		(*result).miss_hit = MISS_STORE;
	}
	else { // loadstore == false (0) es un load
		(*result).miss_hit = MISS_LOAD;
	}
	
	// Iterar por toda la lista, buscando un hit
	for (i = 0; i < associativity; i++){
		// Hay un hit
		if(cache_blocks[i].tag == tag){
			// Actualizar 'end'
			end = i;
			current_dirty = cache_blocks[i].dirty;
			// REVISAR LOADSTORE
			if(loadstore) { // si loadstore == true (1) es un store
				(*result).miss_hit = HIT_STORE;
			}
			else { // loadstore == false (0) es un load
				(*result).miss_hit = HIT_LOAD;
			}
		}
	}
	
	// Obtener bloque a reemplazar
	(*result).evicted_address = cache_blocks[end].tag;
	(*result).dirty_eviction = cache_blocks[end].dirty;
	if( ((*result).miss_hit == HIT_STORE) || ((*result).miss_hit) == HIT_LOAD){
		(*result).dirty_eviction = 0;
	}
	
	// Actualizar todos los bloques posteriores
	for (i = end-1; i >= 0  ; i --){
		cache_blocks[i+1] = cache_blocks[i];
		cache_blocks[i+1].rp_value = cache_blocks[i+1].rp_value + 1;
	}
	
	// Revisar sucio para bloque inicial nuevo
	if( ((*result).miss_hit == MISS_STORE ) || ((*result).miss_hit == HIT_STORE) ) { // si loadstore == true (1) es un store
		cache_blocks[0].dirty = 1;
	}
	else if ((*result).miss_hit == HIT_LOAD){
		cache_blocks[0].dirty = current_dirty;
	}
	else {
		cache_blocks[0].dirty = 0;
	}
	
	// Actualizar bloque 0
	cache_blocks[0].tag = tag;
	cache_blocks[0].valid = 1;
	cache_blocks[0].rp_value = 0;
	
	/*
	for (i = 0; i < associativity ; i ++){
		printf("tag: %x \t",cache_blocks[i].tag);
	}
	printf("\n");*/
	
	return OK;
} 
 
 
int nru_replacement_policy(int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry* cache_blocks,
                           operation_result* result,
                           bool debug){

	//Para test 6, revisar parametros invalidos
	if (idx < 0 ||tag < 0 || associativity < 0){
		return PARAM; 
	}
	//Declaracion de variables
	int found = 0 ; //para determinar si hubo un hit o no 
	int i; //para iterar
	int in_cache = 0; //para saber cuando dejar de iterar
	int end; //para saber en que via se va a guardar

	//se itera por el set buscando un hit 
	// se busca si el dato es valido y si coincide con el tag

	for(i = 0; i < associativity ; i++){
		if (found == 1){
			break;
		}
		else{
			if (cache_blocks[i].tag == tag && cache_blocks[i].valid == true){
				found = 1; //se encontro tag
				result -> dirty_eviction = false; //no se saco ningun valor del cache
				cache_blocks[i].rp_value = 0; //valor de rp para hit = 0
				if(loadstore){ // si loadstore == true (1) es un store
					result -> miss_hit = HIT_STORE;  
				}
				else{ // si loadstore == false (0) es un load 
					result -> miss_hit = HIT_LOAD;
				}  // Actualizar bit de dirty solo si no estaba sucio
				 if (cache_blocks[i].dirty != 1 ) {
                    if (loadstore){
		                    cache_blocks[i].dirty = 1;
	             } 
               } 
			}	
		}		
	}
	// si no hubo un hit
	if(found == 0){

		for (i = 0; i < associativity; i++){
			if(found == 1){
				break;
			}
			else
			{
				if(cache_blocks[i].valid == false){ //si hay espacio disponible en una via
					cache_blocks[i].valid = true;  //se actualiza el bit de valid
					cache_blocks[i].tag = tag;  //se actualiza el tag
					cache_blocks[i].rp_value = 0; //NRU: Cache fill -> se pone rp en 0
					end = i; //para saber donde se almaceno 
					in_cache = 1; //para saber que se almaceno en cache
					found = 1;
					result -> dirty_eviction = false;
				}	
			}
			
			
		}
		// si no se encontro un espacio vacio
		while (in_cache == 0){
			for(i = 0; i < associativity; i++){
				if(found == 1){
					break;
				}
				else{
					if((cache_blocks[i].rp_value == 1)){

						if (cache_blocks[i].dirty == 1){ //se revisa bit de dirty para dirty eviction
							result -> dirty_eviction = true;
						} 
						else{
							result -> dirty_eviction = false;
						}
						result -> evicted_address = cache_blocks[i].tag;
					cache_blocks[i].valid = true; 
					cache_blocks[i].tag = tag; 
					cache_blocks[i].rp_value = 0; //NRU: cuando hay una eviction se pone rp en 0
					end = i; //para saber donde se almaceno 
					in_cache = 1; //para saber que se almaceno en cache
					found = 1;
					}
				}

			}
			if(in_cache == 0){
				for (i = 0; i < associativity; i++){
					cache_blocks[i].rp_value = 1; //si no hay ceros, se ponen todos los rp bit en 1
				}
			}
		}
		//se revisa si hubo un miss load o store, se actualiza bit de dirty
		if(loadstore){ 
				result -> miss_hit = MISS_STORE;
				cache_blocks[end].dirty = true;
			}
			else{
				cache_blocks[end].dirty = false;
				result -> miss_hit = MISS_LOAD;
			}
	}

	if (found == 1){
		return OK; 
	}
	
	return ERROR;
}
