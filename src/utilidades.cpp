// declarar todo al inicio
#include "utilidades.h"

int readInput(char* string) {
	// inicializar caracter temp
	char temp;
	int i, size;
	int salida = 0;
	// iterar sobre todo el string
	size = (int)strlen(string);
	// iterar todo el string
	for (i=0; i<size; ++i) {
		salida = salida*10;
		temp = (int)(string[i]) - '0';
		salida = salida + temp;
	}
	// devolver el nuevo int a la salida
	return salida;
}

input_params getParams(int argc, char* argv[]){
	// Declaración de variables
	int i;
	// Valores por defecto de parámetros por la terminal
	input_params params = {};
	// puntero ptr para argumentos
	char *ptr;
	// lectura de entradas por argv	
	for(i=0; i<argc; i++){
		// obtener puntero correspondiente a este argumento particular
		ptr = argv[i];
		// revisar casos importantes
		if((*ptr=='-')&(*(ptr+1)=='t')){
			// obtener tamaño de cache en kilobytes
			params.t = readInput(argv[i+1]);
		}
		else if((*ptr=='-')&(*(ptr+1)=='l')){
			// obtener tamaño de línea en bytes
			params.l = readInput(argv[i+1]);
		}
		else if((*ptr=='-')&(*(ptr+1)=='a')){
			// obtener asociatividad
			params.a = readInput(argv[i+1]);
		}
		else if((*ptr=='-')&(*(ptr+1)=='r')&(*(ptr+2)=='p')){
			// obtener politica de reemplazo
			params.rp = readInput(argv[i+1]);
		}
	}
	// devolver parametros
	return params;
}

void printParams(input_params params){
	// definir division
	const char* division = "------------------------------------------------------------------------";
	// imprimir contenido
	printf("%s\n%s\n%s\n",division, "Cache parameters:", division );
	printf("Cache Size (KB): \t\t\t\t %d\n", params.t);
	printf("Cache Associativity: \t\t\t\t %d\n", params.a);
	printf("Cache Block Size (bytes): \t\t\t %d\n", params.l);
	// division final
	printf("%s\n", division);
}

address_sizes getSizes(input_params params){
	// declarar struct
	address_sizes sizes = {};
	// obtener tamaños para cache L1
	sizes.offset_bits = (unsigned int) log2f(params.l);
	sizes.index_bits = (unsigned int) log2f((params.t*(1024))/(params.a*params.l)); 
	sizes.tag_bits = 32 - (sizes.index_bits + sizes.offset_bits); //tamaño de dirección = 32 bits, menos índice y offset
	// devolver tamaños
	return sizes;
}

void getAddress(unsigned int input, address_sizes sizes, address* info){
	// obtener información
	(*info).full = input;
	(*info).offset = (*info).full & ((1<< sizes.offset_bits) - 1 );
	(*info).full = (*info).full >> sizes.offset_bits;
	(*info).index = (*info).full & ((1<< sizes.index_bits) -1 );
	(*info).full = (*info).full >> sizes.index_bits;
	(*info).tag = (*info).full & ((1<< sizes.tag_bits) -1 );
	(*info).full = input;
}

