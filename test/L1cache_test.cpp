/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>



class L1cache : public ::testing::Test{
    protected:
	int debug_on;
	virtual void SetUp()
	{
           /* Parse for debug env variable */
	   get_env_var("TEST_DEBUG", &debug_on);
	};
};

/*
 * TEST1: Verifies miss and hit scenarios for srrip policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_srrip){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on,hecking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}

/*
 * TEST2: Verifies miss and hit scenarios for lru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_lru) {
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (int j = 0 ; j < 2; j++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = i;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)j;
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    print_way_info(idx, associativity, cache_line);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on,hecking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    print_way_info(idx, associativity, cache_line);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}

/*
 * TEST3: Verifies miss and hit scenarios for nru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_nru) {
int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on,hecking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
}

/*
 * TEST4: Verifies replacement policy promotion and eviction
 * 1. Choose a random policy 
 * 2. Choose a random associativity
 * 3. Fill a cache entry
 * 4. Insert a new block A
 * 5. Force a hit on A
 * 6. Check rp_value of block A
 * 7. Keep inserting new blocks until A is evicted
 * 8. Check eviction of block A happen after N new blocks were inserted
 * (where N depends of the number of ways)
 */
TEST_F(L1cache, promotion){
	// Variables
	bool loadstore = 1;
	bool debug = 0;
	int status;
	int rp_chosen;
	int i;
	int idx;
	int tag;
	line_info access = random_access(loadstore);
	enum miss_hit_status expected_miss_hit;
	operation_result result = {};
	
	/* Fill a random cache entry */
	idx = rand()%1024;
	tag = rand()%4096;
	
	 /* Generate A */
 	parameters params = random_params();
 	sizes sizes = get_sizes(params);
 	entry_info A = get_entry_info(access.address,sizes);
 	entry_info B = get_entry_info(access.address,sizes);
	
	
	/* Choose a replacement policy */
	rp_chosen = rand() % 3 + 1; 
	printf("Rp chosen: %d ", rp_chosen);
	entry cache_line[params.a];
	
    /* Fill cache line */
	for ( i =  0; i < params.a; i++) {
		cache_line[i].valid = true;
		cache_line[i].tag = rand()%4096;
		cache_line[i].dirty = 0;
		cache_line[i].rp_value = i;
		while (cache_line[i].tag == tag) {
			cache_line[i].tag = rand()%4096;
		}
	} 

	/* Choose replacement_policy */
	if(rp_chosen == 1){ // LRU
		// Insert A
		status = lru_replacement_policy(A.l1_idx, 
										A.l1_tag, 
										A.l1_assoc,
										loadstore,
										cache_line,
										&result,
										(bool)debug_on);
		// Generate B
		while(A.l1_tag == B.l1_tag || is_in_set(cache_line,B.l1_assoc,B.l1_tag)){
			B.l1_tag = rand()%4096;
		}
		// Force hit on A
		status = lru_replacement_policy(A.l1_idx, 
										A.l1_tag, 
										A.l1_assoc,
										loadstore,
										cache_line,
										&result,
										(bool)debug_on);
		// Check expected rp value
		EXPECT_EQ(cache_line[0].rp_value, 0);
		
		// Seguir agregando hasta que se salga
		for (i = 0; i < params.a; i++){
			B = get_entry_info(access.address,sizes);
			while(A.l1_tag == B.l1_tag || A.l1_idx != B.l1_idx || is_in_set(cache_line,B.l1_assoc,B.l1_tag)){
				access = random_access();
				B = get_entry_info(access.address,sizes);
			}	
			status = lru_replacement_policy(B.l1_idx, 
											B.l1_tag, 
											B.l1_assoc,
											loadstore,
											cache_line,
											&result,
											(bool)debug_on);
		}
		/* Check eviction of new block*/
		EXPECT_EQ(is_in_set(cache_line,A.l1_assoc,A.l1_tag), 0);
	}
	else if (rp_chosen == 2){ // nru
		// Insert A
		status = lru_replacement_policy(A.l1_idx, 
										A.l1_tag, 
										A.l1_assoc,
										loadstore,
										cache_line,
										&result,
										(bool)debug_on);
		// Generate B
		while(A.l1_tag == B.l1_tag || is_in_set(cache_line,B.l1_assoc,B.l1_tag)){
			B.l1_tag = rand()%4096;
		}
		// Force hit on A
		status = lru_replacement_policy(A.l1_idx, 
										A.l1_tag, 
										A.l1_assoc,
										loadstore,
										cache_line,
										&result,
										(bool)debug_on);
		// Check expected rp value
		for (i = 0; i < A.l1_assoc; i++){
			if(cache_line[i].tag == A.l1_tag){
				EXPECT_EQ(cache_line[i].rp_value, 0);
			}
		}
		
		// Seguir agregando Bs hasta que A salga
		for (i = 0; i < params.a; i++){
			B = get_entry_info(access.address,sizes);
			while(A.l1_tag == B.l1_tag || A.l1_idx != B.l1_idx || is_in_set(cache_line,B.l1_assoc,B.l1_tag)){
				access = random_access();
				B = get_entry_info(access.address,sizes);
			}	
			status = lru_replacement_policy(B.l1_idx, 
											B.l1_tag, 
											B.l1_assoc,
											loadstore,
											cache_line,
											&result,
											(bool)debug_on);
		}
		/* Check eviction of new block*/
		EXPECT_EQ(is_in_set(cache_line,A.l1_assoc,A.l1_tag), 0);
	}
	else if (rp_chosen == 3){ // srrip
		// Insert A
		status = lru_replacement_policy(A.l1_idx, 
										A.l1_tag, 
										A.l1_assoc,
										loadstore,
										cache_line,
										&result,
										(bool)debug_on);
		// Generate B
		while(A.l1_tag == B.l1_tag || is_in_set(cache_line,B.l1_assoc,B.l1_tag)){
			B.l1_tag = rand()%4096;
		}
		// Force hit on A
		status = lru_replacement_policy(A.l1_idx, 
										A.l1_tag, 
										A.l1_assoc,
										loadstore,
										cache_line,
										&result,
										(bool)debug_on);
		// Check expected rp value
		for (i = 0; i < A.l1_assoc; i++){
			if(cache_line[i].tag == A.l1_tag){
				EXPECT_EQ(cache_line[i].rp_value, 0);
			}
		}
		
		// Seguir agregando Bs hasta que A salga
		for (i = 0; i < params.a; i++){
			B = get_entry_info(access.address,sizes);
			while(A.l1_tag == B.l1_tag || A.l1_idx != B.l1_idx || is_in_set(cache_line,B.l1_assoc,B.l1_tag)){
				access = random_access();
				B = get_entry_info(access.address,sizes);
			}	
			status = lru_replacement_policy(B.l1_idx, 
											B.l1_tag, 
											B.l1_assoc,
											loadstore,
											cache_line,
											&result,
											(bool)debug_on);
		}
		/* Check eviction of new block*/
		EXPECT_EQ(is_in_set(cache_line,A.l1_assoc,A.l1_tag), 0);
	} 
}


/*
 * TEST5: Verifies evicted lines have the dirty bit set accordantly to the operations
 * performed.
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry with only read operations
 * 4. Force a write hit for a random block A
 * 5. Force a read hit for a random block B
 * 6. Force read hit for random block A
 * 7. Insert lines until B is evicted
 * 8. Check dirty_bit for block B is false
 * 9. Insert lines until A is evicted
 * 10. Check dirty bit for block A is true
 */
TEST_F(L1cache, writeback){
int status;
  int i;
  int idx = rand()%1024;
  int tag;
  int tagB;
  int tagA;
  bool loadstore;
  operation_result resultA = {};
  operation_result resultB = {};
  bool debug = 0;
  bool evictedB = 0;
  bool evictedA = 0; 


  // Choose a random policy
  int policy = 2; //  1 << (rand()%4);
  // Choose a random associativity
  int associativity = 1 << (rand()%4);
  // Fill a cache entry with only read operations
  entry cache_line_A[associativity]; // De acá se toma bloque A
  entry cache_line_B[associativity]; // De acá se toma bloque B
  /* Fill cache line A */
    for ( i =  0; i < associativity; i++) {
      loadstore = false; // READ
      cache_line_A[i].valid = true;
      cache_line_A[i].tag = rand()%4096;
      cache_line_A[i].dirty = 0;
       if(policy == 1){
        cache_line_A[i].rp_value = i;
      }
      else if(policy == 2){
        cache_line_A[i].rp_value = 1;
      }
      else if(policy == 3){
        cache_line_A[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      }
    }
     /* Fill cache line B */
    for ( i =  0; i < associativity; i++) {
      loadstore = false; // READ
      cache_line_B[i].valid = true;
      cache_line_B[i].tag = rand()%4096;
      cache_line_B[i].dirty = 0;
       if(policy == 1){
        cache_line_B[i].rp_value = i;
      }
      else if(policy == 2){
        cache_line_B[i].rp_value = 1;
      }
      else if(policy == 3){
        cache_line_B[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      }
    }
  // Forzar write hit en A
     tagA = tag;
     loadstore = 1;
   if (policy == 1){  
   lru_replacement_policy(idx,tag,associativity,loadstore,cache_line_A,&resultA,debug);
   }
  
    if (policy == 2){  
   nru_replacement_policy(idx,tag,associativity,loadstore,cache_line_A,&resultA,debug);
   }
    if (policy == 3){  
   srrip_replacement_policy(idx,tag,associativity,loadstore,cache_line_A,&resultA,debug);
   }
   // Forzar read hit en A
     loadstore = 0;
   if (policy == 1){  
   lru_replacement_policy(idx,tag,associativity,loadstore,cache_line_A,&resultA,debug);
   }
    if (policy == 2){  
   nru_replacement_policy(idx,tag,associativity,loadstore,cache_line_A,&resultA,debug);
    }
    if (policy == 3){  
   srrip_replacement_policy(idx,tag,associativity,loadstore,cache_line_A,&resultA,debug);
   }
  // Insert lines until A is evicted
while (resultA.evicted_address != tagA)
{

    tag = rand()%4096;

  // Seguir forzando miss hasta que A es evicted
  if (policy == 1){  
   lru_replacement_policy(idx,tag,associativity,loadstore,cache_line_A,&resultA,debug);   
   }
   if (policy == 2){  
   nru_replacement_policy(idx,tag,associativity,loadstore,cache_line_A,&resultA,debug);
   }
    if (policy == 3){  
   srrip_replacement_policy(idx,tag,associativity,loadstore,cache_line_A,&resultA,debug);
   }
} 
   // Check dirty_bit for block A is true 
   EXPECT_EQ(resultA.dirty_eviction, true);
   //EXPECT_EQ(resultA.evicted_address, tagA);

   // Forzar read hit en B
   tagB = tag;
  loadstore = 0;
  if (policy == 1){  
   lru_replacement_policy(idx,tag,associativity,loadstore,cache_line_B,&resultB,debug);
   }
    if (policy == 2){  
   nru_replacement_policy(idx,tag,associativity,loadstore,cache_line_B,&resultB,debug);
   }
    if (policy == 3){  
   srrip_replacement_policy(idx,tag,associativity,loadstore,cache_line_B,&resultB,debug);
   }

  // Insert lines until B is evicted
 while (resultB.evicted_address != tagB)
{
    tag = rand()%4096;

  if (policy == 1){  
   lru_replacement_policy(idx,tag,associativity,loadstore,cache_line_B,&resultB,debug);   
   }
   if (policy == 2){  
   nru_replacement_policy(idx,tag,associativity,loadstore,cache_line_B,&resultB,debug);
   }
    if (policy == 3){  
   srrip_replacement_policy(idx,tag,associativity,loadstore,cache_line_B,&resultB,debug);
   }
}
  
   // Check dirty_bit for block B is false 
   EXPECT_EQ(resultB.dirty_eviction, false); 

}

/*
 * TEST6: Verifies an error is return when invalid parameters are pass
 * performed.
 * 1. Choose a random policy 
 * 2. Choose invalid parameters for idx, tag and asociativy
 * 3. Check function returns a PARAM error condition
 */
TEST_F(L1cache, boundaries){
 int status;
  int i;
  int idx;
  int tag;
  int associativity;
  bool loadstore = 1;
  bool debug = 0;
  int rp_chosen;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  //choose a replacement policy 
  rp_chosen = rand() % 3 + 1; 
  printf("Rp chosen: %d ", rp_chosen);
  entry cache_line[associativity];
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = tag;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = 1;
    }
  //llamar a funcion con parametros invalidos (negativos)
  if(rp_chosen == 1){
     status = lru_replacement_policy(-idx, 
                                     -tag, 
                                     -associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
  }
  else if (rp_chosen == 2){
     status = nru_replacement_policy(-idx, 
                                     -tag, 
                                     -associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);    

  }
  else if (rp_chosen == 3){
     status = srrip_replacement_policy(-idx, 
                                     -tag, 
                                     -associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
  }  
  EXPECT_EQ(status, 1); //status is expected to be 1 (PARAM)
}
